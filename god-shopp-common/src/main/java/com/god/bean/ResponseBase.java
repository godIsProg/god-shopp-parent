package com.god.bean;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author yilei
 * @Create 2019-01-05 12:20
 **/
@Getter
@Setter
public class ResponseBase {
    private Integer rtnCode;
    private String msg;
    private Object data;


    public ResponseBase() {
    }

    public ResponseBase(Integer rtnCode, String msg, Object data) {
        this.rtnCode = rtnCode;
        this.msg = msg;
        this.data = data;
    }


}
