package com.god.utils;

import com.god.constants.Constants;

import java.util.UUID;

/**
 * @Author yilei
 * @Create 2019-01-07 21:38
 **/
public class TokenUtils {
    public static String getToken() {
        return Constants.TOKEN_MEMBER + "-" + UUID.randomUUID();
    }
}
