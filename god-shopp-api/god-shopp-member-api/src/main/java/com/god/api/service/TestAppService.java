package com.god.api.service;

import com.god.bean.ResponseBase;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * @Author yilei
 * @Create 2019-01-05 11:02
 **/
@RequestMapping("/member")
public interface TestAppService {
    @RequestMapping("/test")
    Map<String, Object> test(Integer id,String name);

    @RequestMapping("/testResponseBase")
    ResponseBase testResponseBase();

    @RequestMapping("/testRedis")
    ResponseBase setTestRedis(String key,String value);

    @RequestMapping("/getRedis")
    ResponseBase getRedis(String key);

}
