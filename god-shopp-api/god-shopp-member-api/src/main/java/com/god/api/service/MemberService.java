package com.god.api.service;

import com.god.bean.ResponseBase;
import com.god.entity.UserEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author yilei
 * @Create 2019-01-06 11:51
 **/
@RequestMapping("/member")
public interface MemberService {

    @RequestMapping("/findUser")
    ResponseBase findUserById(Long userId);

    @RequestMapping("/register")
     ResponseBase register(@RequestBody UserEntity user);

    @RequestMapping("/login")
    ResponseBase login(@RequestBody UserEntity user);

    @RequestMapping("/findUserByToken")
    ResponseBase findUserByToken(String token);


}
