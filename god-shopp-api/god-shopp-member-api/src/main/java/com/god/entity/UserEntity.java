package com.god.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @Author yilei
 * @Create 2019-01-06 11:49
 **/
@Getter
@Setter
public class UserEntity {

    private Integer id;
    private String username;
    private String password;
    private String phone;
    private String email;
    private Date created;
    private Date updated;

}
