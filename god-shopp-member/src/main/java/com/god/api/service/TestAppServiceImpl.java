package com.god.api.service;

import com.god.api.service.TestAppService;
import com.god.bean.BaseApiService;
import com.god.bean.ResponseBase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author yilei
 * @Create 2019-01-05 11:13
 **/
@RestController
@Slf4j
public class TestAppServiceImpl  extends BaseApiService implements TestAppService{
    @Override
    public Map<String, Object> test(Integer id, String name) {
        Map<String, Object> result = new HashMap<>();
        result.put("errorCode", "200");
        result.put("msg", "success");
        result.put("data", "id=" + id + ",name=" + name);
        log.info("id=" + id + ",name=" + name);
        return result;
    }

    @Override
    public ResponseBase testResponseBase() {
        return super.setResultSuccess();
    }

    @Override
    public ResponseBase setTestRedis(String key, String value) {
        baseRedisService.setString(key,value,null);
        return super.setResultSuccess();
    }

    @Override
    public ResponseBase getRedis(String key) {
        String value = baseRedisService.getString(key);
        return setResultSuccess(value);
    }
}
