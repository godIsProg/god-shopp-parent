package com.god.api.service;

import com.alibaba.fastjson.JSONObject;
import com.god.bean.BaseApiService;
import com.god.bean.ResponseBase;
import com.god.constants.Constants;
import com.god.dao.MemberDao;
import com.god.entity.UserEntity;
import com.god.mq.RegisterMailboxProducer;
import com.god.utils.MD5Util;
import com.god.utils.TokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author yilei
 * @Create 2019-01-06 11:55
 **/
@Slf4j
@RestController
public class MemberServiceImpl extends BaseApiService implements MemberService {

    @Autowired
    MemberDao memberDao;

    @Autowired
    RegisterMailboxProducer registerMailboxProducer;

    @Value("${messages.queue}")
    private String MESSAGESQUEUE;
    @Override
    public ResponseBase findUserById(Long userId) {
        UserEntity userEntity = memberDao.findByID(userId);
        if (userEntity == null) {
            return super.setResultError("未查到用户信息!");
        }
        return super.setResultSuccess(userEntity);
    }

    @Override
    public ResponseBase register(@RequestBody  UserEntity user) {
        String password = user.getPassword();
        if (StringUtils.isEmpty(password)) {
            return super.setResultError("密码不能为空!");
        }
        String newPassword = MD5Util.MD5(password);
        user.setPassword(newPassword);
        Integer result = memberDao.insertUser(user);
        if (result <= 0) {
            return super.setResultError("注册失败!");
        }

        // 采用MQ异步发送邮件
        String email = user.getEmail();
        String messAageJson = message(email);
        log.info("######email:{},messAageJson:{}",email,messAageJson);
        sendMsg(messAageJson);

        return super.setResultSuccess("注册成功!");
    }

    @Override
    public ResponseBase login(@RequestBody UserEntity user) {
        //1.验证参数
        String username = user.getUsername();
        if (StringUtils.isEmpty(username)) {
            return super.setResultError("登录帐号不能为空");
        }
        String password = user.getPassword();
        if (StringUtils.isEmpty(password)) {
            return super.setResultError("登录密码不能为空");
        }
        //2.数据库查询帐号密码是否正确
        String newPassword = MD5Util.MD5(password);
        UserEntity userEntity = memberDao.login(username, newPassword);
        if (userEntity == null) {
            return super.setResultError("帐号或密码不正确");
        }
        //3.如果帐号密码正确,对应生成token
        String token = TokenUtils.getToken();

        //4.存放在token中,key为token,value 为userId
        Integer userId = userEntity.getId();
        baseRedisService.setString(token, userId+"", Constants.TOKEN_MEMBER_TIME);
        //5.返回token
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("memberToken", token);
        return super.setResultSuccess(jsonObject);
    }

    @Override
    public ResponseBase findUserByToken(String token) {
        //1验证参数
        if (StringUtils.isEmpty(token)) {
            return super.setResultError("token不能为空");
        }
        //2 在redis根据token查找userId
        String strUserId = baseRedisService.getString(token);
        if (StringUtils.isEmpty(strUserId)) {
            return super.setResultError("token无效或已过期");
        }
        Long userId = Long.parseLong(strUserId);
        //3 在数据库中根据userId查找用户信息
        UserEntity userEntity = memberDao.findByID(userId);
        if (userEntity == null) {
            return super.setResultError("未查找到该用户信息");

        }
        userEntity.setPassword(null);
        return super.setResultSuccess(userEntity);
    }


    private String message(String mail) {
        JSONObject root = new JSONObject();
        JSONObject header = new JSONObject();
        header.put("interfaceType", Constants.SMS_MAIL);
        JSONObject content = new JSONObject();
        content.put("mail", mail);
        root.put("header", header);
        root.put("content", content);
        return root.toJSONString();
    }

    private void sendMsg(String json) {
        ActiveMQQueue activeMQQueue = new ActiveMQQueue(MESSAGESQUEUE);
        registerMailboxProducer.sendMsg(activeMQQueue, json);
    }


}
